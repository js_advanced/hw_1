Hamburger.SIZE_SMALL = {
    name: "small",
    type: "size",
    price: 50,
    calorie: 20,
};
Hamburger.SIZE_LARGE = {
    name: "large",
    type: "size",
    price: 100,
    calorie: 40,
};
Hamburger.STUFFING_CHEESE = {
    name: "cheese",
    type: "stuffing",
    price: 10,
    calorie: 20,
};
Hamburger.STUFFING_SALAD = {
    name: "salad",
    type: "stuffing",
    price: 20,
    calorie: 5,
};
Hamburger.STUFFING_POTATO = {
    name: "potato",
    type: "stuffing",
    price: 15,
    calorie: 10,
};
Hamburger.TOPPING_MAYO = {
    name: "mayo",
    type: "topping",
    price: 20,
    calorie: 5,
};
Hamburger.TOPPING_SPICE = {
    name: "spice",
    type: "stuffing",
    price: 15,
    calorie: 0,
};

function Hamburger(size, stuffing) {

    try {
        if (!size || size.type !== 'size') {
            throw  new Error('Size is expected!');
        }
        if (!stuffing || stuffing.type !== "stuffing"){
            throw new Error('Stuffing is expected!');
        }

    this._size = size;
    this._stuffing = stuffing;
    this._topping = [];

    } catch (err) {
        alert(err.message)
    }
}
Hamburger.prototype.getSize = function () {
    console.log(this._size.name);
};

Hamburger.prototype.getStuffing = function () {
    console.log(this._stuffing.name);
};

Hamburger.prototype.getToppings = function() {
    console.log(this._topping);
};

Hamburger.prototype.addToppings = function(topping) {
    try {
        if (!topping || (topping.name !== 'mayo' && topping.name !== 'spice') ) {
            throw  new Error('Please, enter topping correctly!');
        }
        this._topping.forEach(function (item) {
                if (item === topping) {
                    throw new Error('This type of topping has been already existed')
                }
        });
        this._topping.push(topping);
    }
    catch (err) {
        alert(err.message);

    }
};

Hamburger.prototype.removeToppings = function(topping) {
    try {
        if (!topping || (topping.name !== 'mayo' && topping.name !== 'spice') ) {
            throw  new Error('Please, enter topping correctly!');
        }
        var isExist;
        this._topping.forEach(function (item) {
            if (item === topping) {
                isExist = true;
            }
        });
        if (isExist) {
            this._topping.splice(this._topping.indexOf(topping),1);
        }
        else  {
            throw new Error('Topping wasn not added');
        }

    }
    catch (err) {
        alert(err.message);

    }
};

Hamburger.prototype.calculatePrice = function () {
    var totalPrice = 0;
    totalPrice += this._size.price;
    totalPrice += this._stuffing.price;
    totalPrice += this._topping.reduce(function (total, item) {
        return total + item.price;
    }, 0);
    console.log(totalPrice);
    return totalPrice;
};

Hamburger.prototype.calculateCalories = function () {
    var totalCalories = 0;
    totalCalories += this._size.calorie;
    totalCalories += this._stuffing.calorie;
    totalCalories += this._topping.reduce(function (total, item) {
        return total + item.calorie;
    }, 0);
    console.log(totalCalories );
    return totalCalories ;
};




var burger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
console.log(burger);

// var burger1 = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.SIZE_LARGE); //error trapping
// console.log(burger1);

burger.getSize();
burger.getStuffing();


// burger.addToppings(Hamburger.TOPPING_MAYO);//error trapping
burger.addToppings(Hamburger.TOPPING_SPICE);
burger.addToppings(Hamburger.TOPPING_MAYO);
burger.removeToppings(Hamburger.TOPPING_MAYO);
// burger.removeToppings(Hamburger.TOPPING_MAYO);//error trapping
burger.getToppings();
burger.calculatePrice();
burger.calculateCalories();


